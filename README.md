# Otus_task6

**Task1**


Основной недостаток установки программ при помощи make install — это неудобное
управление. Вместо этого, сделаю сборку rpm пакетов из исходников. А в качестве
примера буду использовать Nginx 1.17.3 и свежий openssl-1.1.1c, который поддерживает 
алгоритмы шифрования CHACHA20 и Poly1305.

Устанавливаем все необходимое для сборки и компиляции:
```
yum groupinstall -y "Development Tools" && yum install rpmdevtools nano yum-utils
```

Для сборки пакета необходимо создать структуру каталогов. Для этого выполниv команду ниже:
rpmdev-setuptree

Для настройки репозитория yum необходимо создать файл:
`vim /etc/yum.repos.d/nginx.repo`

Копируем в файл следующие строки:
```
[nginx]
name=nginx repo
baseurl=http://nginx.org/packages/mainline/centos/7/$basearch/
gpgcheck=0
enabled=1

[nginx-source]
name=nginx source repo
baseurl=http://nginx.org/packages/mainline/centos/7/SRPMS/
gpgcheck=1
enabled=0
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7
```
Загружаем пакет с исходниками и запускаем установку rmp:
```
mkdir package
cd /package
yumdownloader --source nginx
rpm -Uvh nginx*.src.rpm
```
Здесь он ругается warning: user/group builder does not exist - using root. Создадим для сборки пакета пользователя builder:
```
useradd builder
usermod -a -G builder builder
```

Установим все необходимые для сборки Nginx зависимости:
`yum-builddep nginx`

Скачиваем и распаковываем исходники OpenSSL:
```
cd /usr/src/
wget https://www.openssl.org/source/latest.tar.gz
tar -xvzf latest.tar.gz
```
В параметрах сборки необходимо указать путь к исходникам OpenSSL:
```
vim ~/rpmbuild/SPECS/nginx.spec
--with-openssl=/usr/src/openssl-1.1.1c 
```
Запускаем сборку пакета:
```
cd ~/rpmbuild/SPECS/
rpmbuild -ba nginx.spec

...

Executing(%clean): /bin/sh -e /var/tmp/rpm-tmp.lYMsGJ
+ umask 022
+ cd /root/rpmbuild/BUILD
+ cd nginx-1.17.3
+ /usr/bin/rm -rf /root/rpmbuild/BUILDROOT/nginx-1.17.3-1.el7.ngx.x86_64
+ exit 0
```
После завершения сборки переносим rpm из папки:
```
cd ~/rpmbuild/
tree RPMS/
RPMS/
└── x86_64
    ├── nginx-1.17.3-1.el7.ngx.x86_64.rpm
    └── nginx-debuginfo-1.17.3-1.el7.ngx.x86_64.rpm
cd RPMS/x86_64/    
```
И запускаем установку командой:
```
rpm -Uvh nginx*.rpm
Preparing...                          ################################# [100%]
Updating / installing...
   1:nginx-debuginfo-1:1.17.3-1.el7.ng################################# [ 50%]
   2:nginx-1:1.17.3-1.el7.ngx         ################################# [100%]
----------------------------------------------------------------------

Thanks for using nginx!
```
______________________________________________________________________

**Task2** - ссылка на дз http://35.202.185.193/repo/


Теперь приступим к созданию своего репозитория. Директория для статики у NGINX по
умолчанию `/usr/share/nginx/html`. Создадим там каталог repo:
`mkdir /usr/share/nginx/html/repo`
Копируем туда наш собранный RPM 
`cp rpmbuild/RPMS/x86_64/nginx-1.17.3-1.el7_4.ngx.x86_64.rpm /usr/share/nginx/html/repo/`

и например RPM, для установки репозитория Percona-Server: 
```
wget
http://www.percona.com/downloads/percona-release/redhat/0.1-6/percona-release-0.1-6.noarch.rpm \
-O /usr/share/nginx/html/repo/percona-release-0.1-6.noarch.rpm
```
Инициализируем репозиторий командой:
`createrepo /usr/share/nginx/html/repo/`

Прописываем настройки nginx `autoindex on` и проверяем, у меня почему то именно в этом каталоге еще создается и repodata
```
curl -a localhost/repo/
<html>
<head><title>Index of /repo/</title></head>
<body>
<h1>Index of /repo/</h1><hr><pre><a href="../">../</a>
<a href="repodata/">repodata/</a>                                          31-Aug-2019 19:47                   -
<a href="nginx-1.17.3-1.el7.ngx.x86_64.rpm">nginx-1.17.3-1.el7.ngx.x86_64.rpm</a>                  31-Aug-2019 19:47             3716828
<a href="percona-release-0.1-6.noarch.rpm">percona-release-0.1-6.noarch.rpm</a>                   13-Jun-2018 06:34               14520
</pre><hr></body>
</html>
```

Добавляем репозиторий otus
```
 cat >> /etc/yum.repos.d/otus.repo << EOF
[otus]
name=otus-linux
baseurl=http://localhost/repo
gpgcheck=0
enabled=1
EOF
```
и проверяем его 
```
yum repolist enabled | grep otus
otus                                otus-linux                                 2
```

